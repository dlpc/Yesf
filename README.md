# Yesf

[![License](https://img.shields.io/badge/license-apache2-blue.svg)](LICENSE)

Yesf是一个基于Swoole的PHP框架。提供了以下功能：

* MVC模式和路由分发
* 插件支持
* 多端口监听
* 异步任务

Yesf is a PHP framework based on Swoole. The following functions are provided:

* MVC mode and route distribution
* Plug-in support
* Multi-port monitoring
* Asynchronous task

# 文档

[中文文档](http://yesf.mydoc.io/)

[English documentation](http://yesf-en.mydoc.io/)